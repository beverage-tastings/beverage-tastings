import React from 'react';
import Navigation from './components/Navigation/Navigation'
import Home from './components/Home/Home'
import './App.css';

function App() {
  return (
    <div className="App">
      <Navigation />
      <Home />
    </div>
  );
}

export default App;
