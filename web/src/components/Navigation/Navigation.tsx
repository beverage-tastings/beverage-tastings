import React from 'react';
import 'tachyons'

const Navigation = () => (<nav className='bg-black-60 fixed top-0 w-100'>
            <ul className='dib w-80 fr ma0'>
            <h1 className='no-underline f4 fw7 fl pa3 white hover-bg-white hover-black'>Create an event</h1>
            <h1 className='no-underline f4 fw7 fl pa3 white hover-bg-white hover-black'>My events</h1>
            <h1 className='no-underline f4 fw7 fr pa3 mr4 white hover-bg-white hover-black'>Logout</h1>
            </ul>
        </nav>

)


export default Navigation;