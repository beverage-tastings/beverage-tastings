import React from 'react';
import 'tachyons';

const Home = () => (
    <div className='w-80 center'>
        <div className='pt6'>
            <img className='pa5 fr w-30' src='https://cdn.pixabay.com/photo/2017/01/29/13/20/mobile-devices-2017978_1280.png' alt=""/>
            <article className='pa5 fl w-50'>
                <h2>Beverage Tastings App helps to organize tasting events</h2>
                <p>Prepare your tasting experience, collect and analyse notes, engage with your guests</p>
            </article>
        </div>

        <img className='pa5 fl w-30' src='https://cdn.pixabay.com/photo/2017/02/04/15/25/desk-2037545_1280.png' alt=""/>
        <article className='pa5 fr w-50'>
            <h2>Collect notes from your guests</h2>
            <p>No need for paper anymore. Guests can just use their phones. WSET options for wine and BJCP for beer will come soon.</p>
        </article>

        <img className='pa5 fr w-30' src='https://cdn.pixabay.com/photo/2018/07/15/11/31/online-3539421_1280.png' alt=""/>
        <article className='pa5 fl w-50'>
            <h2>Get live feedback</h2>
            <p>We analyse ratings and notes for you and give you a comprehensive look at what people think.</p>
        </article>

        <img className='pa5 fl w-30' src='https://cdn.pixabay.com/photo/2018/01/31/05/43/web-3120321_1280.png' alt=""/>
        <article className='pa5 fr w-50'>
            <h2>Engage</h2>
            <p>Follow up with personalised messaging and recommendations based on your guests tasting profiles.</p>
        </article>
    </div>
);

export default Home