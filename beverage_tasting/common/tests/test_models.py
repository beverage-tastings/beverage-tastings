from uuid import UUID

import pytest
from django.test import TestCase

from beverage_tasting.common.tests.mocks import MockBase, MockTimestamped, setup_model

pytestmark = pytest.mark.django_db


class TimestampedModelTestCase(TestCase):
    def setUp(self):
        setup_model(MockTimestamped)
        self.model = MockTimestamped()

    def test_has_timestamps(self):
        self.assertTrue(hasattr(self.model, "created_at"))
        self.assertTrue(hasattr(self.model, "updated_at"))


class BaseModelTestCase(TestCase):
    def setUp(self):
        setup_model(MockBase)
        self.model = MockBase()

    def test_id_is_uuid4(self):
        self.assertTrue(hasattr(self.model, "id"))

        try:
            version = UUID(str(self.model.id)).version
            self.assertEqual(version, 4, "UUID for model ID is not version 4.")
        except ValueError:
            self.fail("Model ID is not UUID.")
