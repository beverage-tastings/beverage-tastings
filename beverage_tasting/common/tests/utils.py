import urllib.parse

from django.conf import settings


def get_test_server_url(path: str) -> str:
    return urllib.parse.urljoin(settings.TESTSERVER_URL, path)
