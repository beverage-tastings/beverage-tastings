from django.db import connection

from beverage_tasting.common.models import Base, Timestamped


def setup_model(model):
    """Creates model from abstract model."""
    with connection.schema_editor(atomic=True) as schema_editor:
        schema_editor.create_model(model)


class MockTimestamped(Timestamped):
    """Mock for abstract Timestamped model."""

    class Meta:
        app_label = "beverage_tasting"
        db_table = "mock_timestamped_model"


class MockBase(Base):
    """Mock for abstract Base model."""

    class Meta:
        app_label = "beverage_tasting"
        db_table = "mock_base"
