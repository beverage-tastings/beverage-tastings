import uuid

from django.db import models


class Timestamped(models.Model):
    """Base abstract model for models with `created` and `updated` timestamps."""

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Base(Timestamped):
    """Base model for each model in application."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    class Meta:
        abstract = True
