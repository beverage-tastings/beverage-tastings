from typing import Dict

from django.conf import LazySettings, settings
from rest_framework.request import Request


def settings_context(_request: Request) -> Dict[str, LazySettings]:
    return {"settings": settings}
