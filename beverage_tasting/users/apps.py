from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "beverage_tasting.users"
    verbose_name = _("Users")

    def ready(self) -> None:
        try:
            import beverage_tasting.users.signals  # noqa F401
        except ImportError:
            pass
