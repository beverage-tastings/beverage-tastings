from rest_framework import permissions
from rest_framework.request import Request
from rest_framework.views import APIView

from beverage_tasting.common.models import Base


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow user to access own resource.
    """

    def has_object_permission(self, request: Request, view: APIView, obj: Base) -> bool:
        return obj == request.user
