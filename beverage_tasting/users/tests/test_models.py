from django.test import TestCase

from beverage_tasting.users.tests.factories import UserFactory


class TestUserModel(TestCase):
    def setUp(self):
        self.user = UserFactory.build()

    def test_get_full_name(self):
        self.user.name = "Rhea"
        self.assertEqual(self.user.get_full_name(), "Rhea")

    def test_get_full_name_is_stripped(self):
        self.user.name = "  Rhea  "
        self.assertEqual(self.user.get_full_name(), "Rhea")

    def test_get_short_name(self):
        self.user.name = "Rhea"
        self.assertEqual(self.user.get_short_name(), "Rhea")

    def test_get_short_name_is_stripped(self):
        self.user.name = "  Rhea  "
        self.assertEqual(self.user.get_short_name(), "Rhea")
