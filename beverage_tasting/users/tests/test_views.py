from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from beverage_tasting.users.tests.factories import UserFactory


class UserViewTests(APITestCase):
    def setUp(self):
        self.user = UserFactory.create()
        self.client.force_authenticate(user=self.user)

    def get_detail_url(self, pk=None) -> str:
        pk = pk if pk is not None else self.user.id
        return reverse("api:user-detail", kwargs={"id": str(pk)})

    def test_user_detail_url_exists(self):
        response = self.client.get(self.get_detail_url())

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_detail_returns_user(self):
        url = self.get_detail_url()
        response = self.client.get(url)

        self.assertDictEqual(
            {"id": str(self.user.id), "email": self.user.email}, response.data,
        )

    def test_user_detail_returns_only_authenticated_user(self):
        other_user = UserFactory.create()
        response = self.client.get(self.get_detail_url(pk=other_user.id))

        self.assertNotEqual(self.user, other_user)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertDictEqual(
            response.data,
            {
                "detail": ErrorDetail(
                    string="You do not have permission to perform this action.",
                    code="permission_denied",
                )
            },
        )

    def test_user_me_returns_authenticated_user(self):
        response = self.client.get(reverse("api:user-me"))

        self.assertDictEqual(
            {"id": str(self.user.id), "email": self.user.email}, response.data,
        )
