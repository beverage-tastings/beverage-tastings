from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from beverage_tasting.common.models import Base


class User(AbstractUser, Base):
    email = models.EmailField(_("Unique user email"), unique=True, max_length=255)
    email_verified = models.BooleanField(default=False)
    # First Name and Last Name do not cover name patterns around the globe.
    name = models.CharField(_("Name of User"), blank=True, max_length=255)

    def get_full_name(self) -> str:
        """Override Django default to return single name field we use."""
        return self.name.strip()

    def get_short_name(self) -> str:
        """Override Django default to return single name field we use."""
        return self.get_full_name()
