from io import StringIO
from typing import Optional, Tuple
from xml.dom.minidom import Element, parse

from django.core.management.base import BaseCommand, CommandParser

from beverage_tasting.beverages.models import BeerStyle


class Command(BaseCommand):
    help = """
    Parse BJCP style guide XML file: https://github.com/meanphil/bjcp-guidelines-2015/blob/master/styleguide.xml
    """

    def __init__(self, stdout: Optional[StringIO] = None) -> None:
        super().__init__(stdout=stdout)
        self.style_count = 0
        self.substyle_count = 0

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument("filepath", type=str)

    def handle(self, *args: str, **options: str) -> None:
        self.parse_beer_styles(filepath=options["filepath"])

        self.stdout.write(f"Styles created: {self.style_count}")
        self.stdout.write(f"Substyles created: {self.substyle_count}")

    def parse_beer_styles(self, filepath: str) -> None:
        doc: Element = parse(filepath)

        beer_class = self.find_beer_class(doc=doc)
        if beer_class is not None:
            for style in beer_class.getElementsByTagName("category"):
                self.process_beer_style(style=style)

    def find_beer_class(self, doc: Element) -> Optional[Element]:
        style_classes = doc.getElementsByTagName("styleguide")[0].getElementsByTagName(
            "class"
        )
        for style_class in style_classes:
            if style_class.getAttribute("type") == "beer":
                return style_class
        return None

    def process_beer_style(self, style: Element) -> None:
        code, name, substyles, _ = self.parse_beer_style(style=style)
        style, created = self.handle_style_upsert(code=code, name=name)

        if created:
            self.style_count += 1

        for substyle in substyles:
            self.process_beer_substyle(style=style, substyle=substyle)

    def process_beer_substyle(self, style: BeerStyle, substyle: Element) -> None:
        code, name, _, specialties = self.parse_beer_style(style=substyle)
        if specialties:
            # ignore subcategory and use only specialties, based on Brewer's Friend
            for specialty in specialties:
                _, n, _, _ = self.parse_beer_style(style=specialty)
                # specialty categories use code from their parent (subcategory)
                _, created = self.handle_style_upsert(code=code, name=n, parent=style)
                if created:
                    self.substyle_count += 1
        else:
            _, created = self.handle_style_upsert(code=code, name=name, parent=style)
            if created:
                self.substyle_count += 1

    def parse_beer_style(self, style: Element) -> Tuple[str, str, Element, Element]:
        """Parse data needed to create a beer style."""
        code: str = style.getAttribute("id")
        name: str = style.getElementsByTagName("name")[0].childNodes[0].nodeValue
        substyles: Element = style.getElementsByTagName("subcategory")
        specialties: Element = style.getElementsByTagName("specialty")

        return code, name, substyles, specialties

    def handle_style_upsert(
        self, code: str, name: str, parent: BeerStyle = None
    ) -> Tuple[BeerStyle, bool]:
        """Creates or updates a beer style. Notifies to stdout about every update."""
        obj, created = BeerStyle.objects.update_or_create(
            code=code,
            name=name,
            defaults={"code": code, "name": name, "parent": parent},
        )
        if not created:
            self.stdout.write(
                f"Style `{obj.id}` updated with data: [code={code}, name={name}, parent={parent}]."
            )

        return obj, created
