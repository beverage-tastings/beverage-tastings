from rest_framework import serializers

from beverage_tasting.beverages.models import BeerStyle


class BeerStyleSerializer(serializers.ModelSerializer):
    parent_id = serializers.UUIDField()

    class Meta:
        model = BeerStyle
        fields = ["id", "code", "name", "parent_id"]
