from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.viewsets import ReadOnlyModelViewSet

from beverage_tasting.beverages.api.serializers import BeerStyleSerializer
from beverage_tasting.beverages.models import BeerStyle


class BeerStyleViewSet(ReadOnlyModelViewSet):
    serializer_class = BeerStyleSerializer
    queryset = BeerStyle.objects.all().select_related("parent")
    lookup_field = "id"

    # cache beer styles list for 24h
    @method_decorator(cache_page(60 * 60 * 24))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
