from django.db import models

from beverage_tasting.common.models import Base


class BeerStyle(Base):
    parent = models.ForeignKey(
        "self",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="substyles",
    )
    code = models.CharField("BJCP style code", null=False, blank=False, max_length=3)
    name = models.CharField(max_length=64, null=False, blank=False)

    class Meta:
        db_table = "beer_styles"
        indexes = [
            models.Index(fields=["code"]),
            models.Index(fields=["parent"]),
        ]
        unique_together = ["code", "name"]

    def __str__(self) -> str:
        return f"BeerStyle[id={self.id}, code={self.code}, name={self.name}, parent={self.parent}]"
