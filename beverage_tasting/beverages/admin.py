from django.contrib import admin
from reversion.admin import VersionAdmin

from beverage_tasting.beverages.models import BeerStyle


@admin.register(BeerStyle)
class BeerStyleAdmin(VersionAdmin):
    pass
