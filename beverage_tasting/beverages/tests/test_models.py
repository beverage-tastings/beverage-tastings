import pytest
from django.db import IntegrityError
from django.test import TestCase

from beverage_tasting.beverages.models import BeerStyle
from beverage_tasting.beverages.tests.factories import BeerStyleFactory


class TestBeerStyle(TestCase):
    def test_allow_insert_when_code_and_name_are_unique(self):
        ipa = BeerStyleFactory.create(code="21", name="IPA")
        BeerStyleFactory.create(code="21B", name="Rye IPA", parent=ipa)

        self.assertEqual(BeerStyle.objects.all().count(), 2)

    def test_forbid_insert_when_code_and_name_are_not_unique(self):
        ipa = BeerStyleFactory.create(code="21", name="IPA")
        BeerStyleFactory.create(code="21B", name="Rye IPA", parent=ipa)

        with pytest.raises(IntegrityError):
            BeerStyleFactory.create(code="21B", name="Rye IPA")

    def test_str_representation(self):
        parent_style = BeerStyleFactory.create(code="1", name="Lager")
        child_style = BeerStyleFactory.create(
            code="1A", name="Pilsen Lager", parent=parent_style
        )
        expected_parent_str = (
            f"BeerStyle[id={parent_style.id}, code=1, name=Lager, parent=None]"
        )

        self.assertEqual(str(parent_style), expected_parent_str)
        self.assertEqual(
            str(child_style),
            f"BeerStyle[id={child_style.id}, code=1A, name=Pilsen Lager, parent={expected_parent_str}]",
        )
