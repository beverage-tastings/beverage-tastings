from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from beverage_tasting.beverages.tests.factories import BeerStyleFactory
from beverage_tasting.users.tests.factories import UserFactory


class BeerStyleViewTestCase(APITestCase):
    def setUp(self):
        self.client.force_authenticate(user=UserFactory.build())

    def test_list_beerstyles(self):
        ipa = BeerStyleFactory.create(code="21", name="IPA")
        american_ipa = BeerStyleFactory.create(
            code="21A", name="American IPA", parent=ipa
        )
        belgian_ipa = BeerStyleFactory.create(
            code="21B", name="Belgian IPA", parent=ipa
        )
        rye_ipa = BeerStyleFactory.create(code="21B", name="Rye IPA", parent=ipa)
        url = reverse("api:beerstyle-list")

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertListEqual(
            response.data,
            [
                {"id": str(ipa.id), "code": "21", "name": "IPA", "parent_id": None},
                {
                    "id": str(american_ipa.id),
                    "code": "21A",
                    "name": "American IPA",
                    "parent_id": str(ipa.id),
                },
                {
                    "id": str(belgian_ipa.id),
                    "code": "21B",
                    "name": "Belgian IPA",
                    "parent_id": str(ipa.id),
                },
                {
                    "id": str(rye_ipa.id),
                    "code": "21B",
                    "name": "Rye IPA",
                    "parent_id": str(ipa.id),
                },
            ],
        )

    def test_beerstyle_detail(self):
        beer_style = BeerStyleFactory.create(code="21A", name="American IPA")
        url = reverse("api:beerstyle-detail", kwargs={"id": str(beer_style.id)})

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.data,
            {
                "id": str(beer_style.id),
                "code": "21A",
                "name": "American IPA",
                "parent_id": None,
            },
        )
