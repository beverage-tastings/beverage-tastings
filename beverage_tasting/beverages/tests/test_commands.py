import os
from io import StringIO

from django.core.management import call_command
from django.test import TestCase

from beverage_tasting.beverages.models import BeerStyle


class TestParseBeerStylesCommand(TestCase):
    def test_handle(self):
        out = StringIO()
        filepath = os.path.join(
            os.path.dirname(os.path.abspath(__file__)), "fixtures/styleguide.xml"
        )
        # create existing category to test duplicates are not created
        scottish = BeerStyle.objects.create(code="14", name="Scottish Ale")

        call_command("parsebeerstyles", filepath, stdout=out)
        output = out.getvalue()

        # output informs about count of parsed styles and substyles
        self.assertIn("Styles created: 33", output)
        self.assertIn("Substyles created: 117", output)

        # printed counts reflect the reality in DB
        self.assertEqual(BeerStyle.objects.filter(parent__isnull=True).count(), 34)
        self.assertEqual(BeerStyle.objects.filter(parent__isnull=False).count(), 117)

        # parent relationships are correct
        dark_lager = BeerStyle.objects.get(code="8")
        munich_dunkel = BeerStyle.objects.get(code="8A")
        self.assertEqual(munich_dunkel.parent, dark_lager)

        # parent relationships are correct for specialty type of beer
        ipa_style = BeerStyle.objects.get(code="21")
        specialty_ipa_styles = BeerStyle.objects.filter(code="21B")
        self.assertEqual(len(specialty_ipa_styles), 6)
        for style in specialty_ipa_styles:
            self.assertEqual(style.parent, ipa_style)

        # existing styles are updated, not added as a new
        scottish.refresh_from_db()
        self.assertIn(
            f"Style `{scottish.id}` updated with data: [code=14, name=Scottish Ale, parent=None].",
            output,
        )
