from factory import DjangoModelFactory

from beverage_tasting.beverages.models import BeerStyle


class BeerStyleFactory(DjangoModelFactory):
    class Meta:
        model = BeerStyle
