from django.urls import include, path, reverse
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase

from beverage_tasting.beverages.tests.factories import BeerStyleFactory
from beverage_tasting.users.tests.factories import UserFactory


class BeerStyleURLTestCase(APITestCase, URLPatternsTestCase):
    urlpatterns = [
        path("api/", include("config.api_router")),
    ]

    def setUp(self):
        self.authenticate()

    def authenticate(self):
        self.client.force_authenticate(user=UserFactory.build())

    def test_list_beerstyles(self):
        url = reverse("api:beerstyle-list")

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_beerstyle_detail(self):
        beer_style = BeerStyleFactory.create()
        url = reverse("api:beerstyle-detail", kwargs={"id": str(beer_style.id)})

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("name", response.data)
