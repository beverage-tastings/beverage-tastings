from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class BeveragesConfig(AppConfig):
    name = "beverage_tasting.beverages"
    verbose_name = _("Beverages")
