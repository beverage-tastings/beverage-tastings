
Passwordless Authentication
===========================

This app uses `passwordless authentication <https://github.com/aaronn/django-rest-framework-passwordless>`_.
Advantage of this approach is no need to use any user passwords.
This guide will show you the authentication flow.

Obtain a Callback Token
-----------------------

The first step is to obtain a callback token.
Callback token is later replaced for the long-lived token.
Use following command to obtain it:

.. code-block:: bash

    curl -X POST -d "email=info@tastebeer.org" https://tastebeer.org/auth/email/

Command above will trigger email sending to the address provided.

.. note::

    Local development server does not send real e-mails.
    Instead e-mail HTML is printed to standard output.
    Search for login code there.

.. warning::

    Authentication library currently uses only 6-digit callback tokens.

Obtain an Authorization Token
-----------------------------

Callback token is short lived (15 minutes) and should be exchanged for authorization token:

.. code-block:: bash

    curl -X POST -d "email=info@tastebeer.org&token=531680" https://tastebeer.org/auth/token/

    # returns token
    {"token":"89ae6b76a9ec140a16ff369ef2f16e77f9b2919b"}

.. note::

    This is a moment when user registration took place.
    User e-mail is connected with given token in database.

Using the Authorization Token
-----------------------------
Most of API calls are private, sometimes limited only to the owner user.
Provide obtained authorization token to get access to such resource:

.. code-block:: bash

    curl -i -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -H "Authorization: Token 89ae6b76a9ec140a16ff369ef2f16e77f9b2919b" \
    https://tastebeer.org/api/users/me/
