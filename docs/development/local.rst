
Running Locally With Docker
===========================

This project relies heavily on Docker and Docker Compose.

Prerequisites
-------------
- `Docker <https://docs.docker.com/>`_
- `Docker Compose <https://docs.docker.com/compose/>`_

Build the App
-------------
This command should always be run when something changes either in Dockerfile or in app requirements:

.. code-block:: bash

    docker-compose -f local.yml build

Run the App
-----------
At first build the frontend static files:

.. code-block:: bash

    docker-compose -f web.yml up --build

Command will build the frontend container and use it to generate static files to ``/beverage_tasting/static``.
Django later collects and serves static files from this directory.

Next step is to actually run Django and PostgreSQL:

.. code-block:: bash

    docker-compose -f local.yml up

It might be handy to always start services with build options.
It is fast when cached and only takes longer when changes occurred.
Following command is therefore recommended:

.. code-block:: bash

    docker-compose -f local.yml up --build

Usually only `local.yml` file will be used with Docker Compose.
You can set environment variable pointing to this file:

.. code-block:: bash

    export COMPOSE_FILE=local.yml

Then you can run only:

.. code-block:: bash

    docker-compose up --build

Executing Management Commands
-----------------------------

Run one-off container to perform common Django commands:

.. code-block:: bash

    docker-compose run --rm django python manage.py createsuperuser

Command will create container with only the Django application and destroy itself afterwards.

Environment Configuration
-------------------------

Configuration files for local environment are located in ``.envs/.local``.
Username and password for PostgreSQL in ``.envs/.local/.postgres``
might come handy when setting up remote access, e.g. for `DBeaver <https://dbeaver.io/>`_.


