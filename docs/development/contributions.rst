
Contributions
=============

Your help with development, testing or documentation is very welcomed.
Please, follow this short guide to make the most of it.

Development Contributions
-------------------------

For now it is just a matter of a pull request in our `repository <https://gitlab.com/beverage-tastings/beverage-tastings>`_.


Documentation Contributions
---------------------------

Any contributions should be either in a form of an issue or a pull request in our `repository <https://gitlab.com/beverage-tastings/beverage-tastings>`_.

Use following command to generate the documentation locally:

.. code-block:: bash

    docker-compose -f local.yml run --rm django sphinx-build docs/ docs/_build/html/

Bug Reporting
-------------

Please use issues in our `repository <https://gitlab.com/beverage-tastings/beverage-tastings>`_ to report bugs you found.

.. warning::

    Report any security related bugs to contact@tastebeer.org directly.
