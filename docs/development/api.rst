
API
===

API of this application was created with `Django REST framework <https://www.django-rest-framework.org/>`_.
It is browsable locally at `localhost:8000/api/ <localhost:8000/api/>`_.

Alternatively use cURL if you have authorization token:

.. code-block:: bash

    curl -i -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -H "Authorization: Token 89ae6b76a9ec140a16ff369ef2f16e77f9b2919b" \
    localhost:8000/api/

API endpoints won't be listed here as they are easily accessible on their own.

List All URLs
-------------

Listing all API URLs might be helpful and it can be done easily:

.. code-block:: bash

    docker-compose -f local.yml run --rm django python manage.py show_urls
