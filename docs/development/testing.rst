
Testing
=======

This project aims to high coverage and quality of testing.

Run the following command to test the application:

.. code-block:: bash

    docker-compose -f local.yml run --rm django pytest

Coverage
--------

Run tests with code coverage first:

.. code-block:: bash

    docker-compose -f local.yml run --rm django coverage run -m pytest

Once finished either run ``report`` to see coverage immediately or generate browsable ``html`` files:

.. code-block:: bash

    docker-compose -f local.yml run --rm django coverage report
    docker-compose -f local.yml run --rm django coverage html

Generated HTML report can be found in **coverage_html_report**.
