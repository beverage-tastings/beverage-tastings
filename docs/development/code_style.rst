
Code Style
==========

No specific code style is enforced, but the code must adhere to the rules set by following tools:

- `mypy <http://mypy-lang.org/>`_
- `Black <https://black.readthedocs.io/en/stable/>`_
- `Flake8 <https://flake8.pycqa.org/en/latest/>`_

Run the Lint Check
------------------

Recommended order of running tools mentioned above is as follows:

.. code-block:: bash

    docker-compose -f local.yml run --rm django mypy
    docker-compose -f local.yml run --rm django black .
    docker-compose -f local.yml run --rm django flake8

This order makes the most sense because ``mypy`` fail requires code adjustments.
These are then reformatted with ``Black`` if needed
and ``Flake8`` confirms change validity according to PEP8 recommendations.
