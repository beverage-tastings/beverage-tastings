.. Beverage Tasting documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Beverage Tasting's documentation!
======================================================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    pycharm/configuration
    development/local
    development/authentication
    development/api
    development/testing
    development/code_style
    development/contributions


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
