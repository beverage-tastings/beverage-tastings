from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from beverage_tasting.beverages.api.views import BeerStyleViewSet
from beverage_tasting.users.api.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("beerstyles", BeerStyleViewSet)


app_name = "api"
urlpatterns = router.urls
